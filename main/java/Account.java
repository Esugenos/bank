public class Account {

    String ownerName, currency;
    MonetaryAmount mAmount;

    public Account(String ownerName, String currency) {
        this.currency = currency;
        this.ownerName = ownerName;
        mAmount = new MonetaryAmount(0, this.currency);


    }

    public void deposit(double a) {
        mAmount.addAmount(a);

    }

    public void withdraw(double a) {
        double amount = mAmount.getAmount();
        if (a < amount) {
            mAmount.substractAmount(a);
        } else {
            System.out.println("Il va falloit tailler encore quelques menhirs pour retirer une telle somme...");
        }
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getCurrentBalance() {
        return mAmount.toString();
    }
}
