public class MonetaryAmount {

    double amount;
    String currency;

    public MonetaryAmount(double amount, String currency) {
        this.amount = amount;
        this.currency = currency;

    }

    public void addAmount(double a) {
        amount = amount + a;
    }

    public void substractAmount(double a) {
        amount = amount - a;
    }

    public double getAmount() {
        return amount;
    }

    public String toString() {
        String affichage = "Il vous reste actuellement " + amount +" "+ currency + " sur votre compte bancaire";
        return affichage;
    }
}
