
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AccountTest {

    @Test
    void shouldIHaveGoodName(){
        String ownerName="Astérix";
        Account asterix=new Account(ownerName,"euros");

        Assertions.assertEquals("Astérix",asterix.getOwnerName());


    }
    @Test
    void ShouldIHaveSomeMoneyWithGoodCurrency(){
        Account asterix=new Account("Astérix","sesterces");
        asterix.deposit(500);
        asterix.withdraw(200);

        Assertions.assertEquals(300,asterix.mAmount.getAmount());
        Assertions.assertEquals("Il vous reste actuellement 300.0 sesterces sur votre compte bancaire",asterix.getCurrentBalance());

    }

}
