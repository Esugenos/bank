public class Transaction {

    public static void main(String[] args) {
        Account obelix=new Account("Obélix","sesterces");

        System.out.println(obelix.getCurrentBalance());
        obelix.deposit(500.5);
        System.out.println(obelix.getCurrentBalance());
        obelix.withdraw(125.25);
        System.out.println(obelix.getCurrentBalance());
        obelix.withdraw(1337.0);
        System.out.println(obelix.getCurrentBalance());

    }
}
